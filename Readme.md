# Era of NoSQL

We have seen how relational database has been dominated in the software industry from the beginning, but when the requirements become complex and needed scaling the old relational database couldn't satisfy those requirements and a new generation database has been introduced **NoSQL**.

### What is exactly NoSQL
Some say that NoSQL stands for "non SQL" or “not only SQL.” even though there is no official abbreviation, but we can also say it is pretty much the same as what its name implies. If you want to put it in a proper definition we can say
> A NoSQL database provides a mechanism for storage and retrieval of data that is modeled in means other than the tabular relations used in relational databases.

Yes, that's correct there is no old fashion table structure here.
And want to hear one more happy news? you can say goodbye to our good old SQL query, yes I am serious, you don't have to scratch your head anymore to build a complex query to retrieve a value.

So I know u are already excited to know more about NoSQL but before that let see some of the key difference between R-DBMS and NoSQL.

### Difference between SQL and NoSQL
- **The Scalability**
In the NoSQL database, we use an approach called horizontal scale-out which makes it easy to add or reduce capacity quickly and non-disruptively with commodity hardware.
This eliminates the tremendous cost and complexity of manual sharding. Where R-DBMS vertically scalable. This means that you can increase the load on a single server by increasing things like RAM, CPU, or SSD.
- **The Structure**
SQL databases are table-based on the other hand NoSQL databases are either key-value pairs, document-based, graph databases, or wide-column stores
- **Language**
SQL databases define and manipulate data based on structured query language (SQL). SQL is one of the most versatile and widely-used options available which makes it a safe choice, especially for great complex queries. But SQL requires you to use predefined schemas to determine the structure of your data. which makes it sometimes difficult to use but on other hand, A NoSQL database has a dynamic schema for unstructured data. Data is storing depends upon which NoSQL database is using. This means it can be document-oriented, column-oriented, graph-based, or a KeyValue store. This means that documents do not need a defined schema or structure each document can have its own unique structure.
- **High Availability**
NoSQL databases are designed to give high availability and avoid the complexity that comes with a typical RDBMS architecture that relies on primary and secondary nodes. Some “distributed” NoSQL databases use a masterless architecture that automatically distributes data equally among multiple resources so that the application remains available for both reads and write operations even when one node fails.
- **Property Followed**
SQL follows ACID properties (Atomicity, Consistency, Isolation, and Durability) whereas NoSQL follows the Brewers CAP theorem (Consistency, Availability, and Partition tolerance).

Ok. so I think you got some idea about how NoSQL is different from SQL, or still having some confusions, no problem we got your back, check this image out.

![Diffrence in of SQL and NoSQL](https://miro.medium.com/max/10984/1*CeQrHqjVIesDYAH99fFvOg.png)


all right now its time for us to know about some of the NoSQL databases, so buckle up

### Types of NoSQL Database

**Key-Value databases**
Key-Value is one of the simplest forms to store data in a NoSQL database. A value can be only be retrieved by referencing its unique key, so to make a query for retrieving data is simple here. This type of database is great when you need to store large amounts of data but you don’t need to perform complex queries to retrieve it. Common use cases include storing user preferences or caching.
![Key-Value databases](https://insights-images.thoughtworks.com/nosqlkeyvaluedatabase_0978844942000b7164ba509e339d13ce.png)
[*Redis*](https://redis.io/) and [*DynanoDB*](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html) are popular key-value databases.

**Document Databases**
In Document Database data stored in documents similar to XML, JSON, BSON. Each document contains pairs of fields and values. The values can be strings, numbers, booleans, arrays, or objects, and their structures typically align with objects developers are working within code.
![Document Databases](https://insights-images.thoughtworks.com/nosqldocumentdb_c6c3fe5139f181ca9a4b5abc6b411204.png)
[*MongoDB*](https://www.mongodb.com/2) and [*CouchDB*](https://couchdb.apache.org/) are popular Document databases.

**Wide-Column Stores**
Wide-column stores store data in tables, rows, and dynamic columns. Wide-column stores provide a lot of flexibility over relational databases because each row is not required to have the same columns. Many consider wide-column stores to be two-dimensional key-value databases. Wide-column stores are great for when you need to store large amounts of data and you can predict what your query patterns will be.
![Wide-column Stores](https://insights-images.thoughtworks.com/nosqlcolumnfamily_90a547fcf1885c36c9e82c27f4964cf5.png)
[*Cassandra*](https://cassandra.apache.org/) and [*HBase*](https://hbase.apache.org/) are popular Wide-column Stores.

**Graph Databases**
Graph databases store data in nodes and edges. Nodes typically store information about people, places, and things while edges store information about the relationships between the nodes. Graph databases excel in use cases where you need to traverse relationships to look for patterns such as social networks, fraud detection, and recommendation engines.
![Graph databases](https://insights-images.thoughtworks.com/nosqlgraph1_9b2986b08841107929831e51d0effcc5.png)
[*Neo4j*](https://neo4j.com/) and [*JanusGraph*](https://janusgraph.org/) are popular Graph Databases.

Hmm... I know what you are thinking now How to choose the correct database for your requirement. don't worry let me help you

So if you have
- Simple schema
- High velocity read/write with no frequent updates
- High performance and scalability
- No complex queries involving multiple keys or joins
Go for **Key-Value**

if you have
- Flexible schema with complex querying
- JSON/BSON or XML data formats
- Leverage complex Indexes (multikey, geospatial, full-text search, etc)
- High performance and balanced R: W ratio
Go for **Document Database**

if you have
- High volume of data
- Extreme write speeds with relatively fewer velocity reads
- Data extractions by columns using row keys
- No ad-hoc query patterns, complex indices, or high level of aggregations
Go for **Wide-Column Stores**

or if you have
- Applications requiring traversal between data points
- Ability to store properties of each data point as well as the relationship between them
- Complex queries to determine relationships between data points
- Need to detect patterns between data points
Go for **Graph Database**

I think now you can make a decision about what to use when, but before that, I want to tell you something, every coin has two sides same for NoSQL also. There are some disadvantages in NoSQL, No Don't, Don't give me that look, come on nothing is perfect.

### Disadvantages of NoSQL
NoSQL has the following disadvantages.

- **Narrow focus**
NoSQL databases have very narrow focus as it is mainly designed for storage but it provides very little functionality. Relational databases are a better choice in the field of Transaction Management than NoSQL.
- **Open-source**
NoSQL is an open-source database. There is no reliable standard for NoSQL yet. In other words, two database systems are likely to be unequal.
- **Management challenge**
The purpose of big data tools is to make the management of a large amount of data as simple as possible. But it is not so easy. Data management in NoSQL is much more complex than a relational database. NoSQL, in particular, has a reputation for being challenging to install and even more hectic to manage on a daily basis.
- **GUI is not available**
GUI mode tools to access the database are not flexibly available in the market.
- **Backup**
Backup is a great weak point for some NoSQL databases like MongoDB. MongoDB has no approach for the backup of data in a consistent manner.
- **Large document size**
Some database systems like MongoDB and CouchDB store data in JSON format. This means that documents are quite large (BigData, network bandwidth, speed), and having descriptive key names actually hurts since they increase the document size.

And it's time to say goodbye, hope you got an idea about NoSQL, All the best
##### ROCK IN NoSQL. Bye Bye

















